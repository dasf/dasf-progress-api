# SPDX-FileCopyrightText: 2020-2024 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2021-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

authors:
  # list author information. see
  # https://github.com/citation-file-format/citation-file-format/blob/main/schema-guide.md#definitionsperson
  # for metadata
  - family-names: "Eggert"
    given-names: "Daniel"
    affiliation: "Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences"
    orcid: https://orcid.org/0000-0003-0251-4390
    email: "daniel.eggert@gfz-potsdam.de"
  - family-names: "Sasin"
    given-names: "Adam"
    affiliation: "Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences"
    # orcid: null
    email: "sasin@hu-potsdam.de"
  - family-names: "Sommer"
    given-names: "Philipp S."
    affiliation: "Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences"
    orcid: https://orcid.org/0000-0001-6171-7716
    email: "philipp.sommer@hereon.de"
cff-version: 1.2.0
message: "If you use this software, please cite it using these metadata."
title: "Progress API for the Data Analytics Software Framework (DASF)"
keywords:
  - "digital-earth"
  - "dasf"
  - "pulsar"
  - "gfz"
  - "hzg"
  - "hereon"
  - "hgf"
  - "helmholtz"
  - "remote procedure call"
  - "rpc"
  - "django"
  - "python"
  - "websocket"
  - "progress reporting"

license: 'Apache-2.0'
repository-code: "https://codebase.helmholtz.cloud/dasf/dasf-progress-api"
url: "https://codebase.helmholtz.cloud/dasf/dasf-progress-api"
contact:
  # list maintainer information. see
  # https://github.com/citation-file-format/citation-file-format/blob/main/schema-guide.md#definitionsperson
  # for metadata
  - family-names: "Sommer"
    given-names: "Philipp S."
    affiliation: "Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences"
    orcid: https://orcid.org/0000-0001-6171-7716
    email: "philipp.sommer@hereon.de"
abstract: |
  basic back-end progress api for the data analytics software framework dasf
preferred-citation:
  title: "DASF: A data analytics software framework for distributed environments"
  authors:
    - family-names: Eggert
      given-names: "Daniel"
      affiliation: "Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences"
      orcid: "https://orcid.org/0000-0003-0251-4390"
    - family-names: Sips
      given-names: "Mike"
      affiliation: "Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences"
      orcid: "https://orcid.org/0000-0003-3941-7092"
    - family-names: Sommer
      given-names: "Philipp S."
      affiliation: "Helmholtz-Zentrum Hereon"
      orcid: "https://orcid.org/0000-0001-6171-7716"
    - family-names: Dransch
      given-names: "Doris"
      affiliation: "https://orcid.org/0000-0003-3941-7092"
  year: 2022
  type: article
  doi: "10.21105/joss.04052"
  date-published: 2022-10-13
  journal: Journal of Open Source Software
  volume: 7
  number: 78
  pages: 4052
  publisher:
    name: The Open Journal
  license: CC-BY-4.0
