<!--
SPDX-FileCopyrightText: 2020-2024 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences

SPDX-License-Identifier: Apache-2.0
-->

# DASF message broker for local development

This django project can be used for local development.
