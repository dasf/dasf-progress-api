# SPDX-FileCopyrightText: 2020-2024 Helmholtz Centre Potsdam GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2021-2024 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: Apache-2.0

import time

if __name__ == "__main__":
    from deprogressapi import PrintReport, ProgressReport

    # create the root report
    with ProgressReport(step_message="Init", steps=2) as pr1:
        time.sleep(1)
        # create a sub report
        with pr1.create_subreport(
            step_message="Step 1.1 - Download"
        ) as pr1_sub1:
            # run the corresponding task ...
            time.sleep(1)
        with pr1.create_subreport(
            step_message="Step 1.2 - Process Download", steps=2
        ) as pr1_sub2:
            # create another sub report with pre-determined number of steps
            time.sleep(1)
            # we can also do this without the `with` clause. Then we have to
            # use `submit` during the initialization and run `complete` at the
            # end
            pr1_sub21 = pr1_sub2.create_subreport(
                step_message="Step 1.2.1 - Process Download", submit=True
            )
            time.sleep(1)
            pr1_sub21.complete()
            with pr1_sub2.create_subreport(
                step_message="Step 1.2.2 - Process Download"
            ) as pr1_sub22:
                time.sleep(1)
        time.sleep(1)

    with PrintReport() as report:
        report.print("This is just a simple statement that will be printed!")
        time.sleep(1)
        report.error("This indicates that an error has happened")
        time.sleep(1)
